<?php
/**
 * Copyright 2018 FlatPlan.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * FlatPlan.
 *
 * As with any software that integrates with the FlatPlan platform, your use
 * of this software is subject to the FlatPlan Developer Principles and
 * Policies [http://flatplan.app/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace FlatPlan\Tests;

use FlatPlan\Components\Gallery;
use FlatPlan\Components\Image;
use FlatPlan\Components\Social;
use FlatPlan\Components\Structure;
use FlatPlan\Components\Text;
use FlatPlan\Components\Video;
use FlatPlan\FlatPlan;
use FlatPlan\Tests\FlatPlanTestCredentials as Credentials;
use PHPUnit\Framework\TestCase;
use FlatPlan\Article;

/**
 *  Corresponding Class to test YourClass class
 *
 *  For each class in your library, there should be a corresponding Unit-Test for it
 *  Unit-Tests should be as much as possible independent from other test going on.
 *
 *  @author yourname
 */
final class ArticleTest extends TestCase {

    public function testIsThereAnySyntaxError()
    {
        $article = new Article(1);
        $this->assertTrue(is_object($article));
        unset($article);
    }

    public function testSetTitle()
    {
        $article  = new Article(1);
        $article->setTitle('My Title');
        $json     = $article->getJson();

        $testObj = new \stdClass();
        $testObj->article_id = 1;
        $testObj->language = 'en';
        $testObj->status = 'draft';
        $testObj->title = 'My Title';
        $testObj->sub_title = null;
        $testObj->category = null;
        $testObj->template = null;
        $testObj->metadata = null;
        $testObj->components = array();

        $this->assertEquals(json_encode($testObj), $json);
        unset($article, $json, $testObj);
    }

    public function testFullArticle()
    {
        $article = new Article('00000000-0000-0000-0000-000000000000');
        $article->setTitle('FP-StyleMaster');
        $article->setLanguage('en');
        $article->setMetaData([
            "thumbnailURL"       => "http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475bside-on-b-3.jpg",
            "excerpt"            => "Standfirst or first sentence of article to go here",
            "canonicalURL"       => "https://staging.flatplan.app/",
            "transparentToolbar" => false
        ]);

        $rootContainer = new Structure('header');
        $rootContainer->setIdentifier('FP-StandardComp.header');
        $header = new Structure('header');
        $rootContainer->setComponents($header);
        $subContainer = new Structure('container');
        $title = new Text('title', 'Article &#8220;<i>master</i>&#8221; <b>&#8216;headline&#8217;</b>', 'html');
        $author = new Text('author', 'BY A. N. Other', 'html');
        $divider = new Structure('divider');
        $subContainer->setComponents([
            $title,
            $author,
            $divider
        ]);
        $rootContainer->setComponents($subContainer);
        $article->setComponents($rootContainer);

        $intro = new Text(
            'intro',
            'Intro paragraph. Pellentesque posuere vehicula sem, volutpat blandit augue auctor vel. In hac habitasse platea dictumst. Quisque interdum dignissim lacus, non eleifend erat. Suspendisse leo leo, lobortis sit amet arcu at, gravida tincidunt augue. Nunc eget rhoncus leo. Phasellus et tempus mauris, a ultricies nulla. Proin eu massa ac nunc molestie maximus.',
            'html'
        );
        $article->setComponents($intro);

        $body = new Text(
            'body',
            '<b>Body paragraph 1.</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget nulla malesuada, efficitur augue sit amet, tincidunt dui. Integer dictum maximus sapien, vitae gravida augue laoreet ut. In vel ligula eget magna gravida ultrices et sed leo. Etiam id ullamcorper libero. Sed molestie nibh justo, eget tincidunt augue consectetur eget. Quisque semper et erat vitae accumsan. Pellentesque ut diam ac velit ullamcorper pulvinar in id felis. Quisque sit amet quam lorem. Ut ac ante ut leo fringilla hendrerit a vel mi. Nam ante erat, condimentum sit amet nulla eget, gravida lobortis ex. Praesent congue ac lacus et lobortis. Etiam eu lacinia quam. Phasellus sed iaculis nunc. Sed luctus maximus accumsan.',
            'html'
        );
        $article->setComponents($body);

        $body = new Text(
            'body',
            '<b>Body paragraph 2.</b> Pellentesque posuere vehicula sem, volutpat blandit augue auctor vel. In hac habitasse platea dictumst. Quisque interdum dignissim lacus, non eleifend erat. Suspendisse leo leo, lobortis sit amet arcu at, gravida tincidunt augue. Nunc eget rhoncus leo. Phasellus et tempus mauris, a ultricies nulla. Proin eu massa ac nunc molestie maximus.',
            'html'
        );
        $article->setComponents($body);

        $photo = new Image('photo', 'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475bside-on-b-3.jpg');
        $article->setComponents($photo);

        $caption = new Text('caption', 'Caption. Lorem ipsum dolor sit', 'html');
        $article->setComponents($caption);

        $gallery = new Gallery('gallery');
        $gallery->setComponents([
            new Image(
                'galleryitem',
                'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475bside-on-b-3.jpg',
                'This is the caption for image 1'
            ),
            new Image(
                'galleryitem',
                'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475brear-low-34-b-3.jpg',
                'This is the caption for image 2'
            ),
            new Image(
                'galleryitem',
                'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475bside-on-b-3.jpg',
                'This is the caption for image 3'
            )
        ]);
        $article->setComponents($gallery);

        $mosaic = new Gallery('mosaic');
        $mosaic->setComponents([
            new Image(
                'galleryitem',
                'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475bside-on-b-3.jpg',
                'This is the caption for image 1'
            ),
            new Image(
                'galleryitem',
                'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475brear-low-34-b-3.jpg',
                'This is the caption for image 2'
            ),
            new Image(
                'galleryitem',
                'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475bside-on-b-3.jpg',
                'This is the caption for image 3'
            )
        ]);
        $article->setComponents($mosaic);

        $logo = new Image('logo', 'https://www.seoclerk.com/pics/want39362-17MRmh1464494128.png', 'A. Logo', 'Logo image generic company');
        $article->setComponents($logo);

        $social = new Social('instagram', 'https://www.instagram.com/p/bNd86MSFv6');
        $article->setComponents($social);

        $social = new Social('facebook_post', 'https://www.facebook.com/applemusic/posts/1372231696125877');
        $article->setComponents($social);

        $social = new Social('tweet', 'https://twitter.com/AppStore/status/591656099792683008');
        $article->setComponents($social);

        $heading = new Text('heading1', 'Heading 1. lorem ipsum dolor sit', 'html');
        $article->setComponents($heading);

        $heading = new Text('heading2', 'Heading 2. lorem ipsum dolor sit', 'html');
        $article->setComponents($heading);

        $heading = new Text('heading3', 'Heading 3. lorem ipsum dolor sit', 'html');
        $article->setComponents($heading);

        $heading = new Text('heading4', 'Heading 4. lorem ipsum dolor sit', 'html');
        $article->setComponents($heading);

        $heading = new Text('heading5', 'Heading 5. lorem ipsum dolor sit', 'html');
        $article->setComponents($heading);

        $heading = new Text('heading6', 'Heading 6. lorem ipsum dolor sit', 'html');
        $article->setComponents($heading);

        $container = new Structure('container');
        $container->setIdentifier('FP-StandardComp.quote');
        $divider = new Structure('divider');
        $divider2 = new Structure('divider');
        $quote = new Text('quote', 'Quote. Typical consumers Google the “best” price and think themselves clever', 'html');
        $container->setComponents([
            $divider,
            $quote,
            $divider2
        ]);
        $article->setComponents($container);

        $container = new Structure('container');
        $container->setIdentifier('FP-StandardComp.pullquote');
        $divider = new Structure('divider');
        $divider2 = new Structure('divider');
        $quote = new Text('pullquote', 'Pullquote. Typical consumers Google the “best” price and think themselves clever', 'html');
        $container->setComponents([
            $divider,
            $quote,
            $divider2
        ]);
        $article->setComponents($container);

        $video = new Video('embedwebvideo', 'https://www.youtube.com/embed/2EAq8lzkhlE', 1.777, 'Video');
        $article->setComponents($video);

        $video = new Video('embedwebvideo', 'https://vimeo.com/channels/staffpicks/276738707', 1.777, 'Video');
        $article->setComponents($video);

        $author = new Text('author', 'BY A. N. Author', 'html');
        $article->setComponents($author);

        $byline = new Text('byline', 'BY A. N. Contrubutor', 'html');
        $article->setComponents($byline);

        $container = new Structure('container');
        $container->setIdentifier('FP-CustomComp.callout');
        $subContainer = new Structure('container');
        $subContainer->setIdentifier('FPCallout-b');
        $heading = new Text('heading4', 'Callout Heading. Example', 'html');
        $photo = new Image('photo', 'http://blog.iso50.com/wp-content/uploads/2008/09/windowslivewriterbenedictredgrovepininfarina-1475bside-on-b-3.jpg');
        $subContainer->setComponents([
            $heading,
            $photo
        ]);
        $container->setComponents($subContainer);
        $subContainer = new Structure('container');
        $subContainer->setIdentifier('FPCallout-C');
        $heading = new Text('heading5', 'Callout Inner Container call to action. Example', 'html');
        $subContainer->setComponents($heading);
        $container->setComponents($subContainer);
        $article->setComponents($container);

        $sdk      = new FlatPlan(Credentials::$accessToken, Credentials::$appId, Credentials::$endpoint);
        $response = $sdk->publish($article);
        $this->assertObjectHasAttribute('flatplanId', $response);

    }
}
