<?php
/**
 * Copyright 2018 FlatPlan.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * FlatPlan.
 *
 * As with any software that integrates with the FlatPlan platform, your use
 * of this software is subject to the FlatPlan Developer Principles and
 * Policies [http://flatplan.app/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace FlatPlan\Tests;

use FlatPlan\Article;
use FlatPlan\Components\Structure;
use FlatPlan\Components\Text;
use FlatPlan\FlatPlan;
use PHPUnit\Framework\TestCase;

/**
 *  Corresponding Class to test YourClass class
 *
 *  For each class in your library, there should be a corresponding Unit-Test for it
 *  Unit-Tests should be as much as possible independent from other test going on.
 *
 *  @author yourname
 */
final class ComponentStyleTest extends TestCase {

    public function testNestedStyles()
    {
        $article = new Article(1);

        $style = new \stdClass();
        $style->backgroundColor = 'red';

        $textStyle = new \stdClass();
        $textStyle->fontWeight = 'bold';
        $textStyle->fontColor = 'blue';

        $container    = new Structure('container');
        $body         = new Text('body', 'Hello World', 'none');
        $subContainer = new Structure('container');
        $subContainer->setStyle($style);
        $subBody      = new Text('body', 'I\'m a sub body!', 'html');
        $subBody->setTextStyle($textStyle);
        $subContainer->setComponents($subBody);
        $container->setComponents($body);
        $article->setComponents([$container, $subContainer]);
        $json = $article->getJson();
        $this->assertJson($json);

        unset($article, $container, $body, $json);
    }

    public function testUnicode()
    {
        $sdk   = new FlatPlan(FlatPlanTestCredentials::$accessToken, FlatPlanTestCredentials::$appId);
        $xml   = simplexml_load_file('https://www.90min.com/posts.rss?text=full');
        $items = $xml->channel->item;

        foreach ($items as $item) {
            $id = trim((string) $item->guid);
            $article = new Article($id);
            $article->setTitle(trim((string) $item->title));
            $article->setLanguage('en');

            $container = new Structure('container');

            $title = new Text('title', trim((string) $item->title), 'none');
            $intro = new Text('intro', trim((string) $item->description), 'none');
            $body  = new Text('body', strip_tags(trim((string) $item->children('content', true)), '<p><br><a>'));

            $container->setComponents([$title, $intro, $body]);
            $article->setComponents($container);

            $meta = [
                'datePublished' => new \DateTime()
            ];
            $article->setMetaData($meta);

            $sdk->publish($article);

        }
    }
}
