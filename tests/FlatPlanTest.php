<?php
/**
 * Copyright 2018 FlatPlan.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * FlatPlan.
 *
 * As with any software that integrates with the FlatPlan platform, your use
 * of this software is subject to the FlatPlan Developer Principles and
 * Policies [http://flatplan.app/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace FlatPlan\Tests;

use PHPUnit\Framework\TestCase;
use FlatPlan\Tests\FlatPlanTestCredentials as Credentials;
use FlatPlan\FlatPlan;
use FlatPlan\Article;

/**
*  Corresponding Class to test YourClass class
*
*  For each class in your library, there should be a corresponding Unit-Test for it
*  Unit-Tests should be as much as possible independent from other test going on.
*
*  @author yourname
*/
final class FlatPlanTest extends TestCase {
	
    /**
    * Just check if the YourClass has no syntax error
    *
    * This is just a simple check to make sure your library has no syntax error. This helps you troubleshoot
    * any typo before you even use this library in a real project.
    *
    */
    public function testConnection()
    {
        $sdk = new FlatPlan(Credentials::$accessToken, Credentials::$appId, Credentials::$endpoint);
        $response = $sdk->testConnection();
        $this->assertTrue($response);
        unset($sdk);
    }

    /**
    * Just check if the YourClass has no syntax error
    *
    * This is just a simple check to make sure your library has no syntax error. This helps you troubleshoot
    * any typo before you even use this library in a real project.
    *
    */
    public function testFetchArticles()
    {
        $sdk      = new FlatPlan(Credentials::$accessToken, Credentials::$appId, Credentials::$endpoint);
        $articles = $sdk->getArticles();
        $this->assertTrue(is_array($articles));
        unset($sdk);
    }

    public function testPublish()
    {
        $article = new Article('jk435h', 'live');
        $article->setTitle('My article');
        $sdk = new FlatPlan(Credentials::$accessToken, Credentials::$appId, Credentials::$endpoint);
        $response = $sdk->publish($article);
        $this->assertObjectHasAttribute('publisherId', $response);
    }

    public function testStatusCheck()
    {
        $sdk      = new FlatPlan(Credentials::$accessToken, Credentials::$appId, Credentials::$endpoint);
        $articles = $sdk->getArticles();
        $status   = $sdk->getArticleStatus($articles[0]->article_id);

        $this->assertNotEmpty($status);
    }
}
