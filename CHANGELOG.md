## Changelog

### 3.0.2
- Remove deprecated flags

### 3.0.0
- Update for PHP v8

### 2.4.0
- Add support for campaignData metadata key

### 2.3.0
- Add Podcast and Link Button roles

### 2.2.6
- Add support for Tiktok embeds

### 2.2.5
- Add new localization parameter to articles

### 2.2.3
- Fix bug where empty imageURLs on Audio components were being included and then failing Apple's validation

### 2.2.2
- Corrected capitalization on Audio component imageURL

### 2.2.1
- Fixed some issues with the JSON component

### 2.2.0
- Added the ability to add raw AN JSON as a component. Note that the JSON is only validated to be valid JSON, not to be compatible with ANF.

### 2.1.4
- Version bump for Packagist

### 2.1.3
- Fix a bug where passing an empty imageUrl to the audio component would cause an exception

### 2.1.2
- Add the `isPaid` parameter

### 2.1.1
- Fix bug with incorrect variable name

### 2.1.0
- Add Article Parameters

### 2.0.0
- Change articleId to article_id and subTitle to sub_title to work with new database migrations  
- Update tests

### 1.0.10
- Require image URLs to have a path to pass validation

### 1.0.9
- Update Gallery Component to only include permitted attributes

### 1.0.8
- Fix bug with $stillUrl var not being set in the Video Component

### 1.0.7
- PHP's URL validator was proving too strict, so changed to simply check the string starts with `http`

### 1.0.6
- Fix minor bug truncating styles and textStyles with identifiers

### 1.0.5
- Allow metadata keys to be updated

### 1.0.4
- Fix issue with `DateTime` meta keys not being formatted

### 1.0.3
- Update meta keys `author` and `keywords` to be arrays

### 1.0.2
- Add methods for custom inline styles

### 1.0.1
- Add JSON_UNESCAPED_UNICODE flag to json_encode() calls

### 1.0.0
- Default text format changed to html
- Empty Text components removed
- Empty component arrays removed from Structures

### 0.3.5-beta

- Added support for additional MetaData keys
- Added type checking on the MetaData keys

### 0.3.4-beta

- Added support for behaviours
- Added support for html table component
- Added stillURL attribute for Video components

### 0.3.3-beta

- Added a template mapping. Manually select your template via the SDK.

### 0.3.2-beta

- Fixed some tests

### 0.3.1-beta

- Added ability to update the status of an article
- Changed the `defer` status to a more meaningful string
- Updated tests
- Changed `addComponent` method in FlatPlan\Article to `setComponents` to match earlier improvements in consistency
- `FlatPlan\Article::setComponents` can now accept an array of components

### 0.2.1-beta

- Changed `setItems` and `getItems` to `setComponents` and `getComponents` in the Gallery Component. This improves consistency of naming.
- Added identifiers
- Updated Component style and layout names to include container identifiers where set

### 0.1.1-beta

- Initial release
