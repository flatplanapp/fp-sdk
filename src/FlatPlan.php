<?php namespace FlatPlan;

use GuzzleHttp\Client as Guzzle;

/**
*  This class is the central point to the SDK
*  All communication between the publisher system and the hub will happen through here
*
*  @author Jon Bowes <jon@jbxonline.net>
*/
class FlatPlan {

    private $endpoint        = 'https://staging.flatplan.app/api/';

    private $client;
    private $accessToken;
    private $appId;
    private $statuses        = array('deleted', 'draft', 'published');
    private $allowedStatuses = array('delete', 'draft', 'publish');

    public function __construct($accessToken, $appId, $endpoint = null)
    {
        $this->client      = new Guzzle([
            'base_uri' => (!is_null($endpoint) ? $endpoint : $this->endpoint)
        ]);
        $this->accessToken = $accessToken;
        $this->appId       = $appId;
    }

    private function getHeaders()
    {
        return [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken,
                'Accept'        => 'application/json',
                'X-Application' => $this->appId,
            ],
            'http_errors' => false
        ];
    }

    public function testConnection()
    {
        try {
            $response = $this->client->get(
                'test',
                $this->getHeaders()
            );
            if ($response->getStatusCode() === 200) {
                return true;
            }
            return false;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return false;
        }
    }
    
    /**
     * retrieve a list of articles from FlatPlan
     *
     * @return \stdClass
     */
    public function getArticles($status = null, $page = 1, $num = 10)
    {
        try {
            $options = $this->getHeaders();
            $options['form_params'] = [
                'page'         => (int) $page,
                'itemsPerPage' => (int) $num
            ];
            if (!is_null($status) && in_array($this->statuses, $status)) {
                $options['form_params']['status'] = $status;
            }
            $response = $this->client->get(
                'article',
                $options
            );
            $articles = json_decode((string) $response->getBody());
            return $articles;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            echo $e->getMessage();
        }
    }

    public function getArticleStatus($articleId)
    {
        try {
            $response = $this->client->get(
                'article/status/' . $articleId,
                $this->getHeaders()
            );
            $status   = json_decode((string) $response->getBody());
            return $status;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            echo $e->getMessage();
        }
    }

    public function publish(Article $article)
    {
        $json    = $article->getJson();
        $options = $this->getHeaders();
        $options['form_params'] = ['articleJson' => $json];
        try {
            $response = $this->client->post(
                'article',
                $options
            );

            $idArray = json_decode((string) $response->getBody());
            return $idArray;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            echo $e->getMessage();
        }
    }

    public function update($articleId, $status)
    {
        if (!in_array($status, $this->allowedStatuses)) {
            throw new \ErrorException('Invalid status passed');
        }
        $options = $this->getHeaders();
        try {
            $response = $this->client->get(
                'article/' . $articleId . '/' . $status,
                $options
            );

            $response = json_decode((string) $response->getBody());
            return $response;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            echo $e->getMessage();
        }
    }
}