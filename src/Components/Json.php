<?php

namespace FlatPlan\Components;

class Json extends AbstractComponent {

    protected $json;
    protected $roles = [
        'section'
    ];

    /**
     * @param string $json
     * @return bool
     */
    public function __construct($json)
    {
        if (empty($json)) {
            return false;
        }

        $object = json_decode($json);
        if ($object instanceof \stdClass) {
            $this->setJson($object);
            $this->setRole('section');
        }

        return false;
    }

    private function setJson($json)
    {
        $this->json = $json;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->role = $this->getRole();
        $component->components = [
            $this->json
        ];
        return $component;
    }
}