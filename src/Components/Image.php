<?php namespace FlatPlan\Components;

class Image extends AbstractComponent {

    protected $url;
    protected $caption;
    protected $accessibilityCaption;
    protected $explicitContent;

    protected $roles   = ['figure', 'image', 'logo', 'photo', 'portrait', 'galleryitem'];

    /**
     * @param string $role
     * @param string $url
     * @param string $caption
     * @param string $accessibilityCaption
     * @param bool $explicitContent
     * @return void
     */
    public function __construct($role, $url, $caption = '', $accessibilityCaption = '', $explicitContent = false)
    {
        $this->setUrl($url);
        $this->setRole($role);
        $this->setCaption($caption);
        $this->setAccessibilityCaption($accessibilityCaption);
        $this->setExplicitContent($explicitContent);
    }

    private function setUrl($url)
    {
        $parsedUrl = parse_url($url);
        if (!filter_var($url, FILTER_VALIDATE_URL, [
                'flags' => FILTER_FLAG_PATH_REQUIRED
            ]) || strlen($parsedUrl['path']) <= 1) {
            throw new \ErrorException('Invalid url supplied.');
        }
        $this->url = $url;
    }

    private function getUrl()
    {
        return $this->url;
    }

    private function setCaption($caption)
    {
        $this->caption = $caption;
    }

    private function getCaption()
    {
        return $this->caption;
    }

    private function setAccessibilityCaption($accessibilityCaption)
    {
        $this->accessibilityCaption = $accessibilityCaption;
    }

    private function getAccessibilityCaption()
    {
        return $this->accessibilityCaption;
    }

    private function setExplicitContent($explicitContent)
    {
        $this->explicitContent = $explicitContent;
    }

    private function getExplicitContent()
    {
        return $this->explicitContent;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        // galleryitems do not have a role attribute.
        if ($this->getRole() !== 'galleryitem') {
            $component->role = $this->getRole();
        }
        $component->URL                  = $this->getUrl();
        $component->caption              = $this->getCaption();
        $component->accessibilityCaption = $this->getAccessibilityCaption();
        $component->explicitContent      = $this->getExplicitContent();
        $component->layout               = $this->getLayout();
        $component->style                = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
