<?php namespace FlatPlan\Components;

class Social extends AbstractComponent {

    protected $url;

    protected $roles = ['instagram', 'facebook_post', 'tiktok', 'tweet'];

    /**
     * @param string $role
     * @param string $url
     * @return void
     */
    public function __construct($role, $url)
    {
        $this->setRole($role);
        $this->setUrl($url);
    }

    private function setUrl($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \ErrorException('Invalid url supplied.');
        }
        $this->url = $url;
    }

    private function getUrl()
    {
        return $this->url;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->role   = $this->getRole();
        $component->URL    = $this->getUrl();
        $component->layout    = $this->getLayout();
        $component->style     = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
