<?php namespace FlatPlan\Components;

class Structure extends AbstractComponent {

    protected $components = array();

    protected $roles = ['aside', 'chapter', 'container', 'divider', 'header', 'section', 'stack'];

    /**
     * @param string $role
     * @return void
     */
    public function __construct($role)
    {
        $this->setRole($role);
    }

    public function setComponents($components)
    {
        if (is_array($components)) {
            foreach ($components as $component) {
                if ($component instanceof AbstractComponent && !is_null($component->getRole())) {
                    $component->updateStyles($this->role);
                    array_push($this->components, $component);
                }
            }
        } else if ($components instanceof AbstractComponent && !is_null($components->getRole())) {
            $components->updateStyles($this->role);
            array_push($this->components, $components);
        }
    }

    private function getComponents()
    {
        $componentList = array();
        foreach ($this->components as $subComponent) {
            array_push($componentList, $subComponent->getComponent());
        }
        return $componentList;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        if ($this->getIdentifier() !== null) {
            $component->identifier = $this->getIdentifier();
        }
        $component->role       = $this->getRole();
        $components            = $this->getComponents();
        if (!empty($components)) {
            $component->components = $this->getComponents();
        }
        $component->layout     = $this->getLayout();
        $component->style      = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
