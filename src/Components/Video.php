<?php namespace FlatPlan\Components;

class Video extends AbstractComponent {

    protected $url;
    protected $aspectRatio;
    protected $caption;
    protected $accessibilityCaption;
    protected $explicitContent;
    protected $stillUrl;

    protected $roles   = ['video', 'embedwebvideo', 'embedvideo'];

    /**
     * @param string $role
     * @param string $url
     * @param float $aspectRatio
     * @param string $caption
     * @param string $accessibilityCaption
     * @param bool $explicitContent
     * @param string $stillUrl
     * @return void
     */
    public function __construct($role, $url, $aspectRatio = 1.777, $caption = '', $accessibilityCaption = '', $explicitContent = false, $stillUrl = null)
    {
        $this->setRole($role);
        $this->setUrl($url);
        $this->setAspectRatio($aspectRatio);
        $this->setCaption($caption);
        $this->setAccessibilityCaption($accessibilityCaption);
        $this->setExplicitContent($explicitContent);
        if (!is_null($stillUrl)) {
            $this->setStillUrl($stillUrl);
        }
    }

    private function setUrl($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \ErrorException('Invalid url supplied.');
        }
        $this->url = $url;
    }

    private function getUrl()
    {
        return $this->url;
    }

    private function setAspectRatio($aspectRatio)
    {
        $this->aspectRatio = (float) $aspectRatio;
    }

    private function getAspectRatio()
    {
        return $this->aspectRatio;
    }

    private function setCaption($caption)
    {
        $this->caption = $caption;
    }

    private function getCaption()
    {
        return $this->caption;
    }

    private function setAccessibilityCaption($accessibilityCaption)
    {
        $this->accessibilityCaption = $accessibilityCaption;
    }

    private function getAccessibilityCaption()
    {
        return $this->accessibilityCaption;
    }

    private function setExplicitContent($explicitContent)
    {
        $this->explicitContent = $explicitContent;
    }

    private function getExplicitContent()
    {
        return $this->explicitContent;
    }

    private function setStillUrl($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \ErrorException('Invalid still url supplied.');
        }
        $this->stillUrl = $url;
    }

    private function getStillUrl()
    {
        return $this->stillUrl;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->URL                  = $this->getUrl();
        $component->role                 = $this->getRole();
        $component->aspectRatio          = $this->getAspectRatio();
        $component->caption              = $this->getCaption();
        $component->accessibilityCaption = $this->getAccessibilityCaption();
        $component->explicitContent      = $this->getExplicitContent();
        if ($component->role === 'video') {
            $component->stillURL = $this->getStillUrl();
        }
        $component->layout    = $this->getLayout();
        $component->style     = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
