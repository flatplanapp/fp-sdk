<?php namespace FlatPlan\Components;

use FlatPlan\Components\Image;

class Gallery extends AbstractComponent {

    protected $components = array();

    protected $roles = ['gallery', 'mosaic'];

    /**
     * @param string $role
     * @return void
     */
    public function __construct($role)
    {
        $this->setRole($role);
    }

    public function setComponents($components)
    {
        if (is_array($components)) {
            foreach ($components as $component) {
                if ($component instanceof Image) {
                    $component->updateStyles($this->role);
                    array_push($this->components, $component);
                }
            }
        } else if ($components instanceof Image) {
            $components->updateStyles($this->role);
            array_push($this->components, $components);
        }
    }

    private function getComponents()
    {
        $componentList = array();
        foreach ($this->components as $subComponent) {
            $imageComponent = $subComponent->getComponent();
            $image          = new \stdClass();
            $image->URL     = $imageComponent->URL;
            $image->caption = $imageComponent->caption;
            array_push($componentList, $image);
        }
        return $componentList;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->role   = $this->getRole();
        $component->items  = $this->getComponents();
        $component->layout = $this->getLayout();
        $component->style  = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
