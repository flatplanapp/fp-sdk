<?php namespace FlatPlan\Components;

class Table extends AbstractComponent {

    protected $data;

    protected $roles = ['htmltable'];

    /**
     * @param string $role
     * @return void
     */
    public function __construct($role, $data)
    {
        $this->setRole($role);
        $this->setData($data);
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->role      = $this->getRole();
        $component->data      = $this->getData();
        $component->layout    = $this->getLayout();
        $component->style     = $this->getStyle();
        $component->textStyle = $this->getTextStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
