<?php namespace FlatPlan\Components;

class Ad extends AbstractComponent {

    protected $bannerType;

    protected $roles       = ['banner_advertisement', 'medium_rectangle_advertisement'];
    protected $bannerTypes = ['any', 'standard', 'double_height', 'large'];

    /**
     * @param string $role
     * @param string $bannerType
     * @return void
     */
    public function __construct($role, $bannerType = null)
    {
        $this->setRole($role);
        if ($role === 'banner_advertisement') {
            $this->setBannerType($bannerType);
        }
    }

    private function setBannerType($bannerType = null)
    {
        if (!in_array($bannerType, $this->bannerTypes)) {
            throw new \ErrorException('Invalid banner type supplied.');
        }
        $this->bannerTypes = $bannerType;
    }

    private function getBannerType()
    {
        return $this->bannerType;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->role       = $this->getRole();
        $component->bannerType = $this->getBannerType();
        $component->layout     = $this->getLayout();
        $component->style      = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
