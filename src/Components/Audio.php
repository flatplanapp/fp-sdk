<?php namespace FlatPlan\Components;

class Audio extends AbstractComponent {

    protected $url;
    protected $caption;
    protected $accessibilityCaption;
    protected $explicitContent;
    protected $imageUrl;

    protected $roles   = ['audio', 'music', 'podcast'];

    /**
     * @param string $role
     * @param string $url
     * @param string $caption
     * @param string $accessibilityCaption
     * @param bool $explicitContent
     * @param string $imageUrl
     * @return void
     */
    public function __construct($role, $url, $caption = '', $accessibilityCaption = '', $explicitContent = false, $imageUrl = '')
    {
        $this->setRole($role);
        $this->setUrl($url);
        $this->setCaption($caption);
        $this->setAccessibilityCaption($accessibilityCaption);
        $this->setExplicitContent($explicitContent);
        $this->setImageUrl($imageUrl);
    }

    private function setUrl($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \ErrorException('Invalid url supplied.');
        }
        $this->url = $url;
    }

    protected function getUrl()
    {
        return $this->url;
    }

    private function setCaption($caption)
    {
        $this->caption = $caption;
    }

    private function getCaption()
    {
        return $this->caption;
    }

    private function setAccessibilityCaption($accessibilityCaption)
    {
        $this->accessibilityCaption = $accessibilityCaption;
    }

    private function getAccessibilityCaption()
    {
        return $this->accessibilityCaption;
    }

    private function setExplicitContent($explicitContent)
    {
        $this->explicitContent = $explicitContent;
    }

    private function getExplicitContent()
    {
        return $this->explicitContent;
    }

    private function setImageUrl($imageUrl)
    {
        if (!empty($imageUrl) && !filter_var($imageUrl, FILTER_VALIDATE_URL)) {
            throw new \ErrorException('Invalid url supplied.');
        }
        $this->imageUrl = $imageUrl;
    }

    private function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->URL                  = $this->getUrl();
        $component->role                 = $this->getRole();
        if ($this->getRole() !== 'podcast') {
            $component->caption              = $this->getCaption();
            $component->accessibilityCaption = $this->getAccessibilityCaption();
            $component->explicitContent      = $this->getExplicitContent();
            $imageUrl = $this->getImageUrl();
            if (!empty($imageUrl) && substr($imageUrl, 0, 4) === 'http') {
                // only add valid image urls
                $component->imageURL = $this->getImageUrl();
            }
        }

        $component->layout               = $this->getLayout();
        $component->style                = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
