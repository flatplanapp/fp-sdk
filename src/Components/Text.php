<?php namespace FlatPlan\Components;

class Text extends AbstractComponent {

    protected $text;
    protected $format;

    protected $formats = ['html', 'markdown', 'none'];
    protected $roles   = [
        'body',
        'title',
        'heading',
        'heading1',
        'heading2',
        'heading3',
        'heading4',
        'heading5',
        'heading6',
        'intro',
        'caption',
        'author',
        'byline',
        'illustrator',
        'photographer',
        'quote',
        'pullquote',
        'link_button'
    ];

    /**
     * @param string $role
     * @param string $text
     * @param string $format
     * @param string $url
     * @return void
     */
    public function __construct($role, $text, $format = 'html', $url = null)
    {
        if (empty($text)) {
            return false;
        }

        if ($role === 'link_button') {
            if (is_null($url)) {
                return false;
            }
            $this->setUrl($url);
        }

        $this->setRole($role);
        $this->setText($text);
        $this->setFormat($format);
    }

    private function setText($text)
    {
        $this->text = $text;
    }

    private function getText()
    {
        return $this->text;
    }

    private function setFormat($format = null)
    {
        if (!in_array($format, $this->formats)) {
            throw new \ErrorException('Invalid format supplied.');
        }
        $this->format = $format;
    }

    private function getFormat()
    {
        return $this->format;
    }

    private function setUrl($url)
    {
        $this->url = $url;
    }

    private function getUrl()
    {
        return $this->url;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->role      = $this->getRole();
        $component->text      = $this->getText();
        if ($this->getRole() === 'link_button') {
            $component->URL = $this->getUrl();
        } else {
            $component->format = $this->getFormat();
        }
        $component->layout    = $this->getLayout();
        $component->style     = $this->getStyle();
        $component->textStyle = $this->getTextStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
