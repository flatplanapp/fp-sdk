<?php namespace FlatPlan\Components;

class Location extends AbstractComponent {

    protected $mapType;
    protected $caption;
    protected $accessibilityCaption;
    protected $latitude;
    protected $longitude;
    protected $items;
    protected $span;

    protected $roles    = ['map', 'place'];
    protected $mapTypes = ['standard', 'hybrid', 'satellite'];

    /**
     * @param string $role
     * @param string $mapType
     * @param string $caption
     * @param string $accessibilityCaption
     * @param string $latitude
     * @param string $longitude
     * @param array $items
     * @param array $span
     * @return void
     */
    public function __construct($role, $mapType = 'standard', $caption = '', $accessibilityCaption = '', $latitude = null, $longitude = null, $items = array(), $span = array())
    {
        $this->setRole($role);
        $this->setMapType($mapType);
        $this->setCaption($caption);
        $this->setAccessibilityCaption($accessibilityCaption);
        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
        $this->setItems($items);
        $this->setSpan($span);
    }

    private function setMapType($mapType)
    {
        if (!in_array($mapType, $this->mapTypes)) {
            throw new \ErrorException('Invalid map type supplied.');
        }
        $this->mapType = $mapType;
    }

    private function getMapType()
    {
        return $this->mapType;
    }

    private function setCaption($caption)
    {
        $this->caption = $caption;
    }

    private function getCaption()
    {
        return $this->caption;
    }

    private function setAccessibilityCaption($accessibilityCaption)
    {
        $this->accessibilityCaption = $accessibilityCaption;
    }

    private function getAccessibilityCaption()
    {
        return $this->accessibilityCaption;
    }

    private function setLatitude($latitude)
    {
        $this->latitude = (float) $latitude;
    }

    private function getLatitude()
    {
        return $this->latitude;
    }

    private function setLongitude($longitude)
    {
        $this->longitude = (float) $longitude;
    }

    private function getLongitude()
    {
        return $this->longitude;
    }

    private function setItems($items = array())
    {
        if (is_array($items) && !empty($items)) {
            $pins = array();
            foreach ($items as $item) {
                $pin = new \stdClass();
                $pin->latitude  = (float) $item['latitude'];
                $pin->longitude = (float) $item['longitude'];
                if (isset($item['caption'])) {
                    $pin->caption = $item['caption'];
                }
                array_push($pins, $pin);
            }
            $this->items = $pins;
        }
    }

    private function getItems()
    {
        return $this->items;
    }

    private function setSpan($span = array())
    {
        if (is_array($span) && count($span) === 2) {
            $mapSpan = new \stdClass();
            $mapSpan->latitudeDelta = (float) $span['latitudeDelta'];
            $mapSpan->longitudeDelta = (float) $span['longitudeDelta'];
            $this->span = $mapSpan;
        }
    }

    private function getSpan()
    {
        return $this->span;
    }

    public function getComponent()
    {
        $component = new \stdClass();
        $component->role                 = $this->getRole();
        $component->mapType              = $this->getMapType();
        $component->caption              = $this->getCaption();
        $component->accessibilityCaption = $this->getAccessibilityCaption();
        $component->latitude             = $this->getLatitude();
        $component->longitude            = $this->getLongitude();
        $component->items                = $this->getItems();
        $component->span                 = $this->getSpan();
        $component->layout               = $this->getLayout();
        $component->style                = $this->getStyle();
        if (!is_null($this->behaviour)) {
            $component->behaviour = $this->getBehaviour();
        }
        return $component;
    }
}
