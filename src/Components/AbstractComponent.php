<?php namespace FlatPlan\Components;

abstract class AbstractComponent {

    protected $role;
    protected $identifier        = null;
    protected $currentIdentifier = null;
    protected $layout            = 'layout';
    protected $style             = 'style';
    protected $customStyle       = null;
    protected $textStyle         = 'textStyle';
    protected $customTextStyle   = null;
    protected $inlineTextStyle   = null;
    protected $behaviour         = null;
    protected $behaviours        = ['motion', 'background_motion', 'parallax', 'background_parallax', 'springy'];

    abstract public function getComponent();

    public function setRole($role)
    {
        if (!in_array($role, $this->roles)) {
            throw new \ErrorException('Invalid role supplied.');
        }
        $this->role = $role;
        $this->updateStyles($role);
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier        = $identifier;
        $this->currentIdentifier = $identifier;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setBehaviour($type, $factor = null)
    {
        if (!in_array($type, $this->behaviours)) {
            throw new \ErrorException('Invalid behaviour supplied.');
        }
        $behaviour = new \stdClass();
        $behaviour->type = $type;
        if (!is_null($factor)  && $type === 'parallax') {
            $behaviour->factor = (float) $factor;
        }
        $this->behaviour = $behaviour;
    }

    public function getBehaviour()
    {
        return $this->behaviour;
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function setStyle($style)
    {
        if (is_object($style)) {
            $this->customStyle = $style;
        }
    }

    public function getStyle()
    {
        return !is_null($this->customStyle) ? $this->customStyle : $this->style;
    }

    public function setTextStyle($textStyle)
    {
        if (is_object($textStyle)) {
            $this->customTextStyle = $textStyle;
        };
    }

    public function getTextStyle()
    {
        return !is_null($this->customTextStyle) ? $this->customTextStyle : $this->textStyle;
    }

    public function updateStyles($role)
    {
        if ($this->currentIdentifier !== null && strpos($this->layout, $this->currentIdentifier)) {
            $this->layout    = substr($this->layout, 0, -(strlen($this->currentIdentifier) + 1));
            $this->style     = substr($this->style, 0, -(strlen($this->currentIdentifier) + 1));
            $this->textStyle = substr($this->textStyle, 0, -(strlen($this->currentIdentifier) + 1));
        }

        $this->layout    = $role . ucfirst($this->layout);
        $this->style     = $role . ucfirst($this->style);
        $this->textStyle = $role . ucfirst($this->textStyle);

        if ($this->currentIdentifier !== null) {
            $this->layout    .= '-' . $this->currentIdentifier;
            $this->style     .= '-' . $this->currentIdentifier;
            $this->textStyle .= '-' . $this->currentIdentifier;
        }

        if (isset($this->components)) {
            foreach ($this->components as $component) {
                if ($this->currentIdentifier !== null && $component->identifier === null) {
                    $component->currentIdentifier = $this->currentIdentifier;
                }
                $component->updateStyles($role);
            }
        }
    }

    public function __toString()
    {
        return json_encode($this->getComponent(), JSON_UNESCAPED_UNICODE);
    }
}
