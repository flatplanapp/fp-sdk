<?php namespace FlatPlan;

use FlatPlan\Components\AbstractComponent;

class Article {

    private $components = array();
    private $status;
    private $article_id;
    private $title;
    private $sub_title;
    private $language = 'en';
    private $category;
    private $template;
    private $metaData = null;
    private $parameters = null;
    private $localization = null;
    private $allowedMetaKeys = array(
        'authors' => 'array',
        'campaignData' => 'array',
        'canonicalURL' => 'uri',
        'coverArt' => 'array',
        'dateCreated' => 'datetime',
        'dateModified' => 'datetime',
        'datePublished' => 'datetime',
        'excerpt' => 'string',
        'keywords' => 'array',
        'links' => 'array',
        'thumbnailURL' => 'uri',
        'transparentToolbar' => 'boolean',
        'videoURL' => 'uri',
        'accessoryText' => 'string',
        'isCandidateToBeFeatured' => 'boolean',
        'isHidden' => 'boolean',
        'isPreview' => 'boolean',
        'isSponsored' => 'boolean',
        'maturityRating' => 'string'
    );
    private $allowedParameters = array(
        'accessoryText' => 'string',
        'isCandidateToBeFeatured' => 'boolean',
        'isHidden' => 'boolean',
        'isPreview' => 'boolean',
        'isSponsored' => 'boolean',
        'maturityRating' => 'string',
        'targetTerritoryCountryCodes' => 'string',
        'isPaid' => 'boolean'
    );
    private $allowedLocalization = array(
        'region' => 'string',
    );

    public function __construct($articleId, $status = 'draft')
    {
        $this->article_id = $articleId;
        $this->status    = $status;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setSubTitle($subTitle) {
        $this->sub_title = $subTitle;
    }

    public function getSubTitle()
    {
        return $this->sub_title;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setMetaData($metaData)
    {
        $metaObj = $this->getMetaData();
        if (is_null($metaObj)) {
            $metaObj = new \stdClass();
        }

        $errors  = array();
        if (is_array($metaData)) {
            foreach ($metaData as $key => $value) {
                if (isset($this->allowedMetaKeys[$key])) {
                    $type = gettype($value);
                    switch ($this->allowedMetaKeys[$key]) {
                        case 'uri':
                            if (substr($value, 0, 4) === 'http') {
                                $metaObj->{$key} = $value;
                            } else {
                                $errors[] = array(
                                    'key' => $key,
                                    'message' => 'Not a valid URI'
                                );
                            }
                            break;

                        case 'datetime':
                            if ($value instanceof \DateTime) {
                                $metaObj->{$key} = $value->format('c');
                            } else {
                                $errors[] = array(
                                    'key' => $key,
                                    'message' => 'Not a valid DateTime object'
                                );
                            }
                            break;

                        case 'array':
                            if (is_array($value)) {
                                $metaObj->{$key} = $value;
                            } else {
                                $errors[] = array(
                                    'key' => $key,
                                    'message' => 'Not a valid Array'
                                );
                            }
                            break;

                        default:
                            if ($type === $this->allowedMetaKeys[$key]) {
                                $metaObj->{$key} = $value;
                            } else {
                                $errors[] = array(
                                    'key' => $key,
                                    'message' => 'Expected ' . $this->allowedMetaKeys[$key] . '; received ' . $type
                                );
                            }
                            break;
                    }
                } else {
                    $errors[] = array(
                        'key' => $key,
                        'message' => 'Not a valid MetaData key'
                    );
                }
            }
        }

        if (!empty($errors)) {
            throw new \ErrorException('Invalid MetaData: ' . print_r($errors, true));
        }

        $this->metaData = $metaObj;
    }

    public function getMetaData()
    {
        return $this->metaData;
    }

    public function setParameters($parameters)
    {
        $parameterObj = $this->getParameters();
        if (is_null($parameterObj)) {
            $parameterObj = new \stdClass();
        }

        $errors = array();
        if (is_array($parameters)) {
            foreach ($parameters as $key => $value) {
                if (isset($this->allowedParameters[$key])) {
                    $type = gettype($value);
                    if ($type === $this->allowedParameters[$key]) {
                        $parameterObj->{$key} = $value;
                    } else {
                        $errors[] = array(
                            'key' => $key,
                            'message' => 'Expected ' . $this->allowedParameters[$key] . '; received ' . $type
                        );
                    }
                } else {
                    $errors[] = array(
                        'key' => $key,
                        'message' => 'Not a valid Parameter'
                    );
                }
            }
        }

        if (!empty($errors)) {
            throw new \ErrorException('Invalid Parameters: ' . print_r($errors, true));
        }

        $this->parameters = $parameterObj;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function setLocalization($localization)
    {
        $localizationObj = $this->getLocalization();
        if (is_null($localizationObj)) {
            $localizationObj = new \stdClass();
        }

        $errors = array();
        if (is_array($localization)) {
            foreach ($localization as $key => $value) {
                if (isset($this->allowedLocalization[$key])) {
                    $type = gettype($value);
                    if ($type === $this->allowedLocalization[$key]) {
                        $localizationObj->{$key} = $value;
                    } else {
                        $errors[] = array(
                            'key' => $key,
                            'message' => 'Expected ' . $this->allowedLocalization[$key] . '; received ' . $type
                        );
                    }
                } else {
                    $errors[] = array(
                        'key' => $key,
                        'message' => 'Not a valid Localization Parameter'
                    );
                }
            }
        }

        if (!empty($errors)) {
            throw new \ErrorException('Invalid Localization Parameters: ' . print_r($errors, true));
        }

        $this->localization = $localizationObj;
    }

    public function getLocalization()
    {
        return $this->localization;
    }

    public function getJson()
    {
        $article               = new \stdClass();
        $article->article_id   = $this->article_id;
        $article->language     = $this->getLanguage();
        $article->status       = $this->status;
        $article->title        = $this->getTitle();
        $article->sub_title    = $this->getSubTitle();
        $article->category     = $this->getCategory();
        $article->template     = $this->getTemplate();
        $article->metadata     = $this->getMetaData();
        $article->parameters   = $this->getParameters();
        $article->localization = $this->getLocalization();
        $article->components   = $this->getComponents();

        return json_encode($article, JSON_UNESCAPED_UNICODE);
    }

    public function getComponents($format = null)
    {
        $output = array();
        foreach ($this->components as $component) {
            array_push($output, $component->getComponent());
        }

        if ($format === 'json') {
            return json_encode($output, JSON_UNESCAPED_UNICODE);
        }

        return $output;
    }

    /**
     * @param AbstractComponent $component
     * @throws \ErrorException
     * @return void
     */
    public function setComponents($components)
    {
        if (is_array($components)) {
            foreach ($components as $component) {
                if ($component instanceof AbstractComponent && !is_null($component->getRole())) {
                    $component->updateStyles('root');
                    array_push($this->components, $component);
                }
            }
        } else if ($components instanceof AbstractComponent && !is_null($components->getRole())) {
            $components->updateStyles('root');
            array_push($this->components, $components);
        }
    }
}
