# FlatPlan SDK for PHP (v3.0.2)

[![Build Status](https://scrutinizer-ci.com/b/flatplanapp/flatplan-sdk/badges/build.png?b=master)](https://scrutinizer-ci.com/b/flatplanapp/flatplan-sdk/build-status/master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/flatplanapp/flatplan-sdk/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/flatplanapp/flatplan-sdk/?branch=master)
[![Latest Stable Version](http://img.shields.io/badge/Latest%20Stable-3.0.2-orange.svg)](https://packagist.org/packages/flatplanapp/flatplansdk)

This repository contains the open source PHP SDK that allows you to access the FlatPlan Platform from your PHP CMS.

## Installation

The FlatPlan PHP SDK can be installed with [Composer](https://getcomposer.org/). Run this command:

```sh
composer require flatplanapp/flatplansdk
```

Please be aware, that whilst in beta there is always the possibility you will come across occasional bugs. 
Please report any issues you come across to the team using the issue tracker on 
[Bitbucket](https://bitbucket.org/flatplanapp/flatplan-sdk/issues)

## API Credentials

In order to access the platform via this SDK, you will need an Access Token and Application ID. These can be obtained 
by emailing [accounts@flatplan.app](mailto:accounts@flatplan.app). These credentials will need to be kept safe and 
passed to the FlatPlan constructor as in the example below. If at any point you believe your access token has been 
compromised, you should email [accounts@flatplan.app](mailto:accounts@flatplan.app) to receive details on how to 
generate a new token.

## Usage

> **Note:** This version of the FlatPlan SDK for PHP requires PHP v8.0 or greater.

Simple GET example of a user's articles.

```php
require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$fp = new FlatPlan\FlatPlan(
  $accessToken,
  $appId
);

// Get a list of articles associated with the current application
// By default, this will return the 10 most recent articles sent to FlatPlan
$articles = $fp->getArticles();

foreach ($articles as $article):
    echo '<strong>Article ID:</strong> ' . $article->id . '<br>' . PHP_EOL;
    echo '<strong>Article Title:</strong> ' . $article->title . '<br>' . PHP_EOL;
    echo '<strong>Article Status:</strong> ' . $article->status . '<br><br>' . PHP_EOL;
endforeach;
```

Complete documentation, installation instructions, and examples are available [here](https://bitbucket.org/flatplanapp/flatplan-sdk/wiki/).

## Tests

1. [Composer](https://getcomposer.org/) is a prerequisite for running the tests. Install composer globally, then run `composer install` to install required files.
2. Create a test app on [FlatPlan](https://hub.flatplan.app), then create `tests/FlatPlanTestCredentials.php` from `tests/FlatPlanTestCredentials.php.dist` and edit it to add your credentials.
3. The tests can be executed by running this command from the root directory:

```bash
$ ./vendor/bin/phpunit
```

By default the tests will send live HTTP requests to the FlatPlan API.
